import { SlateElement } from '@wangeditor/editor'
import { MentionElement } from './custom-types'

// 生成 html 的函数
function mentionToHtml (elem: SlateElement, childrenHtml: string): string {
  const { value = '', varId = '', varName = '', isNotNull = '' } = elem as MentionElement
  return `<span data-w-e-type="mention" data-w-e-is-void data-w-e-is-inline data-content-var="1" data-is-not-null="${isNotNull}" data-var-name="${varName}" data-var-id="${varId}" data-value="${value}">${value}</span>`
}

// 配置
const conf = {
  type: 'mention', // 节点 type ，重要！！！
  elemToHtml: mentionToHtml
}

export default conf
