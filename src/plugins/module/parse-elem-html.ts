import { DOMElement } from './dom'
import { IDomEditor, SlateDescendant, SlateElement } from '@wangeditor/editor'
import { MentionElement } from './custom-types'

function parseHtml (
  elem: DOMElement,
  children: SlateDescendant[],
  editor: IDomEditor
): SlateElement {
  // elem HTML 结构 <span data-w-e-type="mention" data-w-e-is-void data-w-e-is-inline data-value="张三" data-info="xxx">@张三</span>

  const value = elem.getAttribute('data-value') || ''
  const varId = elem.getAttribute('data-var-id') || ''
  const varName = elem.getAttribute('data-var-name') || ''
  const isNotNull = elem.getAttribute('data-is-not-null') || ''

  return {
    type: 'mention',
    value,
    varId,
    varName,
    isNotNull,
    children: [{ text: '' }] // void node 必须有一个空白 text
  } as MentionElement
}

const parseHtmlConf = {
  selector: 'span[data-w-e-type="mention"]',
  parseElemHtml: parseHtml
}

export default parseHtmlConf
