
type EmptyText = {
  text: ''
}
export type MentionElement = {
  varId: string // 变量id
  varName: string // 变量预览值
  isNotNull: any // 是否必填 0 必填；1非必填
  type: 'mention'
  value: string
  info: any
  children: EmptyText[] // void 元素必须有一个空 text
}
  